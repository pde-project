\chapter{Неявная разностная схема}

\section{Построение неявной разностной схемы}

Построим простейшую неявную разностную схему, аппроксимирующую задачу \eqref{eq:problem}, на уже введенной нами сетке \eqref{eq:grid}. От \eqref{eq:scheme:explicit} она отличается лишь тем, что в правой части уравнения сеточная функция $u_{ij}^k$ берется на $(k+1)$-м временном слое, чем  устраняется некоторое <<запаздывание>>, которое имело место в \eqref{eq:scheme:explicit}. Существует много способов построить неявную схему. Используем, например, вот такую.

\begin{equation}
  \left\{
    \label{eq:scheme:implicit}
    \begin{array}{l}
      \begin{array}{lr}
        \frac{1}{c^2} \frac{u_{ij}^{k+1} - 2u_{ij}^{k} + u_{ij}^{k-1}}{h_t^2} =
        \frac{u_{i+1j}^{k} - 2u_{ij}^{k} + u_{i-1j}^{k}}{h_y^2} + \frac{u_{ij+1}^{k+1} - 2u_{ij}^{k+1} + u_{ij-1}^{k+1}}{h_z^2},&
        i = \overline{1, I-1},\\
        &j=\overline{1, J-1},\\
        &k=\overline{1,K-1};\\
      \end{array}\\
      \begin{array}{rclrclr}
        u_{0j}^k &=& 0, & u_{Ij}^k &=& 0, &j=\overline{1,J-1},k=\overline{0,K};\\
        %&&&&&&k=\overline{0,K};\\
        u_{i0}^k &=& \sin\frac{\pi i}{I}\sin\frac{2\pi c k h_t}{\lambda}, & u_{iJ} &=& 0, &i=\overline{0,I},k=\overline{0,K};\\
        %&&&&&&k=\overline{0,K};\\
        u_{ij}^0 &=& 0, & \frac{u_{ij}^1 - u_{ij}^0}{h_t} &=& 0,&i=\overline{1,I-1},j=\overline{1,J-1}.\\
        %&&&&&&j=\overline{1,J-1}.
      \end{array}
    \end{array}
  \right.
\end{equation}

В \eqref{eq:scheme:implicit} мы оставили <<запаздываение>> по одной из пространственных координат, чтобы упростить дальнейшее исследование и, главное, моделирование на ЭВМ.

\section{Аппроксимация неявной разностной схемой}

Теперь покажем, что \eqref{eq:scheme:implicit} аппроксимирует задачу \eqref{eq:problem} и найдем порядки аппроксимации.

Большинство рассуждений из раздела \ref{sec:approx:explicit} остаются справедливыми и здесь. Т.~е. нам нужно показать, что выполняются условия \eqref{eq:approx:1} и \eqref{eq:approx:2}. Первое из них справедливо по очевидным уже указанным в пункте \ref{sec:approx:explicit} причинам. Рассмотрим, как ведет себя норма невязки. Для этого оценим невязку покомпонентно.
\begin{multline}
  \label{eq:implicit:discrepancy:1}
  \delta f^1_h = \left\{
    \frac{1}{c^2}\frac{u(y_i, z_j, t_{k+1}) - 2u(y_i, z_j, t_k) + u(y_i, z_j, t_{k-1})}{h_t^2} - \right. \\
    - \frac{u(y_{i+1}, z_j, t_{k}) - 2u(y_i, z_j, t_{k}) + u(y_{i-1}, z_j, t_{k})}{h_y^2} - \\
    \left. - \frac{u(y_{i}, z_{j+1}, t_{k+1}) - 2u(y_i, z_j, t_{k+1}) + u(y_{i}, z_{j-1}, t_{k+1})}{h_y^2} \right\}.
\end{multline}

Разложим каждое слагаемое в выражении \eqref{eq:implicit:discrepancy:1} невязки по формуле Тейлора в окрестностях некоторой точки
$(y_i, z_j, t_{k})$ для $i = \overline{1, I-1}$, $j = \overline{1, J-1}$, $k = \overline{1, K-1}$ аналогично \eqref{eq:item:1}. В полученном выражении вместо $u(y_i, z_j, t_{k})$ будем писать просто $u$. Вспомнив
вид разложения \eqref{eq:item:1}, можем сразу записать первые два слагаемых в разложенном виде
\begin{multline}
  \label{eq:implicit:item:1:1}
  \delta f^1_h = \left\{
    \frac{1}{c^2}\left(u''_{tt} + u^{IV}_{t^4}\frac{h_t^2}{12} + O(h_t^3)\right) -
    \left( u''_{yy} + u^{IV}_{y^4}\frac{h_y^2}{12} + O(h_y^3) \right) - \right.\\
  \left.
    - \left( u''_{zz}(t_{k+1}) + u^{IV}_{z^4}(t_{k+1})\frac{h_z^2}{12} + O(h_z^3) \right)
    %\phantom{\frac{1}{c^2}}
  \right\}.
\end{multline}

Осталось разложить последнее слагаемое \eqref{eq:implicit:item:1:1}, представляющее собой разложение второй частной производной
по $z$ в точках $(y_i, z_j)$, в точке $t_k$ по формуле Тейлора. Запишем это разложение отдельно.
\begin{equation}
  \label{eq:implicit:decomp:z}
  \begin{array}{rcl}
    u''_{zz}(t_{k+1}) &=& u''_{zz} + u'''_{zzt}h_t + u^{IV}_{zztt}\frac{h_t^2}{2} + O(h_t^3);\\
    u^{IV}_{z^4}(t_{k+1}) &=& u^{IV}_{z^4} + u^{V}_{z^4t}h_t + O(h_t^2).
  \end{array}
\end{equation}

Подставим \eqref{eq:implicit:decomp:z} в \eqref{eq:implicit:item:1:1} и получим полное разложение выражения невязки в точке $(y_i, z_j, t_k)$.

\begin{multline}
  \label{eq:implicit:item:1}
  \delta f^1_h = \left\{
    \frac{1}{c^2}\left(u''_{tt} + u^{IV}_{t^4}\frac{h_t^2}{12} + O(h_t^3)\right) -
    \left( u''_{yy} + u^{IV}_{y^4}\frac{h_y^2}{12} + O(h_y^3) \right) - \right.\\
  \left.
    - \left( u''_{zz} + u'''_{zzt}h_t + u^{IV}_{zztt}\frac{h_t^2}{2} + u^{IV}_{z^4}\frac{h_z^2}{12} + u^{V}_{z^4t}h_t\frac{h_z^2}{12}
       + O(h_t^3h_z^2) + O(h_z^3) \right)
    %\phantom{\frac{1}{c^2}}
  \right\}.
\end{multline}

Вспомним, что в исходной задаче математической физики наблюдалось выражение $u''_{tt} = c^2(u''_{yy} + u''_{zz})$ (соответственно, 
$u^{IV}_{t^4} = c^2(u^{IV}_{yytt} + u^{IV}_{zztt})$. Подставим эти отношения в \eqref{eq:implicit:item:1} и получим
\begin{multline}
  \label{eq:implicit:discrepancy:1:local}
  \delta f_h^1|_{y_i, z_j, t_k} = \Big\{
    u^{IV}_{yytt}\frac{h_t^2}{12} - u^{IV}_{y^4}\frac{h_y^2}{12} - u'''_{zzt}h_t - u^{IV}_{zztt}\frac{5h_t^2}{12}
    - u^{V}_{z^4t}h_t\frac{h_z^2}{12} +\\
  + O(h_t^3) + O(h_y^3) + O(h_z^3) + O(h_t^3h_z^2)
  \Big\}.
\end{multline}

Выделим в \eqref{eq:implicit:discrepancy:1:local} ненулевые слагаемые наименьших порядков относительно шагов дискретизации и получим, что 
локальная аппроксимация первого соотношения неявной схемы \eqref{eq:scheme:implicit} имеет порядки $O(h_t, h_y^2, h_z^2)$.

Аналогично рассуждениям раздела \ref{sec:approx:explicit}, нас интересует еще только одна компонента невязки, ибо все остальные равны нулю. Это седьмая компонента, и для нее получается выражение аналогичное \eqref{eq:discrepancy:7:final}, т.~е. ее порядок аппроксимации $O(h_t)$. Окончательно получаем выражение для погрешности аппроксимации, аналогичное \eqref{eq:approx:explicit}, то есть
\begin{equation}
  \label{eq:approx:imlicit}
  \Vert \delta f_h \Vert_{f_h} = O(h_t, h_y^2, h_z^2).
\end{equation}

\section{Устойчивость неявной разностной схемы}

Исследуем устойчивость неявной разностной схемы подобно тому, как мы делали это для явной схемы в разделе \ref{sec:stable:explicit}. Снова воспользуемся необходимым признаком Неймана и зададимся целью проверить \eqref{eq:neumann}.

Снова полагаем, что $u_{ij}^k$ выбрана специальным образом так, как указано в \eqref{eq:neumann:u}. После подстановки в рекуррентное выражение из \eqref{eq:scheme:explicit} получаем выражение относительно собственных чисел $\lambda(\alpha, \beta)$.

\begin{multline}
\label{eq:u_to_recurr:implicit}
\frac{1}{c^2 h_t^2} \left( \lambda^{k+1} e^{\hat{i} (\alpha i + \beta j)} - 2 \lambda^{k} e^{\hat{i} (\alpha i + \beta j)} + \lambda^{k-1} e^{\hat{i} (\alpha i + \beta j)} \right) = \\
\frac{1}{h_y^2} \left( \lambda^{k} e^{\hat{i} (\alpha (i+1) + \beta j)} - 2 \lambda^{k} e^{\hat{i} (\alpha i + \beta j)} + \lambda^{k} e^{\hat{i} (\alpha (i-1) + \beta j)} \right) + \\
\frac{1}{h_y^2} \left( \lambda^{k+1} e^{\hat{i} (\alpha i + \beta (j-1))} - 2 \lambda^{k+1} e^{\hat{i} (\alpha i + \beta j)} + \lambda^{k+1} e^{\hat{i} (\alpha i + \beta (j-1))} \right)
\end{multline}

Далее поступаем как и ранее, т.~е. разделим \eqref{eq:u_to_recurr:implicit} на $\lambda^{k-1} e^{\hat{i} (\alpha i + \beta j)}$ и используем обозначения \eqref{eq:gamma}. Получаем квадратное уравнение относительно $\lambda$.

\begin{equation}
\label{eq:lambda_eq:implicit:temp}
\lambda^2 - 2 \lambda + 1 = \lambda \gamma_y \left( e^{\hat{i} \alpha i} - 2 + e^{- \hat{i} \alpha i} \right) + \lambda^2 \gamma_z \left( e^{\hat{i} \beta j} - 2 + e^{- \hat{i} \beta j} \right).
\end{equation}

После преобразований \eqref{eq:lambda_eq:implicit:temp} перепишется в виде
\begin{equation}
\label{eq:lambda_eq:implicit}
\lambda^2 \left(4 \gamma_z \sin^2 \frac{\beta j}{2} + 1 \right) + 2 \lambda \left(2 \gamma_y \sin^2 \frac{\alpha i}{2} - 1 \right) + 1 = 0.
\end{equation}

Проанализируем корни \eqref{eq:lambda_eq:implicit} аналогично тому, как мы сделали это для \eqref{eq:lambda_eq}. Рассмотрим случай, когда 
дискриминант уравнения \eqref{eq:lambda_eq:implicit} неположителен.
Дискриминант \eqref{eq:lambda_eq:implicit} представляет собой
\begin{equation}
\label{eq:implicit:discriminant}
D = 4 \left(2 \gamma_y \sin^2 \frac{\alpha i}{2} - 1 \right)^2 - 4 \left(4 \gamma_z \sin^2 \frac{\beta j}{2} + 1 \right).
\end{equation}

Поскольку $\left(4 \gamma_z \sin^2 \frac{\beta j}{2} + 1 \right) \ge 1$, можно утверждать, что
\[
-1 \le \left(2 \gamma_y \sin^2 \frac{\alpha i}{2} - 1 \right) \le 1.
\]

Отсюда мы получаем, что дискриминант \eqref{eq:implicit:discriminant} будет гарантированно отрицательным, если выполняется условие
\begin{equation}
\label{eq:implicit:neg_discr}
\gamma_y < 1.
\end{equation}

В случае выполнения \eqref{eq:implicit:neg_discr} мы получим два комплексно-сопряженных корня, которые меньше единицы по модулю.
Из теоремы Виета получаем соотношение
\begin{equation}
  \label{eq:implicit:lambda_eq:relation}
  |\lambda| = \sqrt{\lambda\cdot\overline{\lambda}} = \frac{1}{\sqrt{1 + 4\gamma_z \sin^2\frac{\beta}{2}}}.
\end{equation}

Учитывая \eqref{eq:implicit:neg_discr} и положительность $\gamma_z$, можно утверждать, что $0 < |\lambda| < 1$.
Это значит, что для рассмотренного случая \eqref{eq:implicit:neg_discr} признак Неймана выполняется.

Рассмотрим другой случай
\begin{equation}
  \label{eq:implicit:nonpos_discr}
  \gamma_y = 1.
\end{equation}

Тогда  дискриминант $D = 0$ и уравнение имеет один корень кратности два, не превышающий по модулю единицу (опять же, по теореме Виета).
\begin{equation}
  \label{eq:implicit:lambda_eq:real}
  |\lambda| = \sqrt{\lambda^2} = \frac{1}{\sqrt{1 + 4\gamma_z \sin^2\frac{\beta}{2}}}.
\end{equation}

Значит, для условия \eqref{eq:implicit:nonpos_discr} условие Неймана также выполняется.

Последняя область для исследования
\begin{equation}
  \label{eq:implicit:pos_discr}
  \gamma_y > 1.
\end{equation}

Тогда дискриминант \eqref{eq:implicit:discriminant} положителен и уравнение имеет два вещественных корня, один из которых по модулю больше единицы, а другой~--- меньше. Условие признака Неймана \eqref{eq:neumann} не выполняется.

Таким образом, получаем, что условие признака Неймана выполняется при
\begin{equation}
  \label{eq:implicit:neuman_final}
  \gamma_y \le 1.
\end{equation}

В силу высокой чувствительности признака Неймана для нашей задачи, мы можем декларировать достаточность условия \eqref{eq:implicit:neuman_final} для устойчивости задачи \eqref{eq:scheme:implicit}.

\section{Моделирование процесса на ЭВМ с помощью неявной разностной схемы}

Выразим из разностного уравнения задачи \eqref{eq:scheme:implicit} значения функции $u_{ij}^k$ на новом временном слое.

\begin{equation}
\label{eq:implicit:resolve}
- \frac{1}{h_z^2} u_{ij+1}^{k+1} + \left( \frac{1}{c^2 h_t^2} + \frac{2}{h_z^2}\right) u_{ij}^{k+1} - \frac{1}{h_z^2} u_{ij-1}^{k+1} =
\frac{u_{i+1j}^{k} - 2u_{ij}^{k} + u_{i-1j}^{k}}{h_y^2} + \frac{2u_{ij}^k - u_{ij}^{k-1}}{c^2 h_t^2}.
\end{equation}

\eqref{eq:implicit:resolve} справедливо для $k = \overline{1, K-1}$. Из краевых условий \eqref{eq:scheme:implicit} нам уже известны значения функции $u_{ij}^k$ на временных слоях $k = 0$ и $k = 1$. На каждом следующем временном слое \eqref{eq:implicit:resolve} дает $(J - 1)$ линейных уравнений для $j = \overline{1, j-1}$. Эти уравнения образуют систему с трехдиагональной матрицей, которая легко решается методом прогонки. Таким образом, слой за слоем мы получаем решение нашей задачи.

Поступая аналогично тому, как мы поступали в разделе \ref{sec:explicit:emulation}, изобразим графики численных решений по неявной схеме.

Неявная схема оказывается довольно капризной в смысле требований. Так взяв не слишком мелкую сетку по времени, получаем решение, мало похожее на правду, но при этом вполне устойчивое (рис.~\ref{pic:implicit:6}). Измельчая сетку по времени, наблюдаем улучшение качества решения (риc.~\ref{pic:implicit:7}). Для пущей убедительности измельчим сетку ещё немного, дабы продемонстрировать сходимость численного решения к аналитическому (риc.~\ref{pic:implicit:8}).

\begin{figure}[!hbtp]
  \centering
  \includegraphics[width=\linewidth]{graph/solution/6}
  \caption{График решения по неявной схеме. $I = 10$, $J = 200$, $T = 1 \cdot 10^{-14}$, $h_t = 1 \cdot 10^{-15}$}
  \label{pic:implicit:6}
\end{figure}

\begin{figure}[!hbtp]
  \centering
  \includegraphics[width=\linewidth]{graph/solution/7}
  \caption{График решения по неявной схеме. $I = 10$, $J = 200$, $T = 1 \cdot 10^{-14}$, $h_t = 1 \cdot 10^{-16}$}
  \label{pic:implicit:7}
\end{figure}

\begin{figure}[!hbtp]
  \centering
  \includegraphics[width=\linewidth]{graph/solution/8}
  \caption{График решения по неявной схеме. $I = 10$, $J = 200$, $T = 1 \cdot 10^{-14}$, $h_t = 1 \cdot 10^{-17}$}
  \label{pic:implicit:8}
\end{figure}

Промоделируем волновой процесс для большего промежутка времени и изобразим результат на рисунке \ref{pic:implicit:9}. Снова можем наблюдать плохую сходимость. При измельчении сетки получаем более адекватный результат, изображенный на рисунке \ref{pic:implicit:10}.

\begin{figure}[!hbtp]
  \centering
  \includegraphics[width=\linewidth]{graph/solution/9}
  \caption{График решения по неявной схеме. $I = 10$, $J = 200$, $T = 4 \cdot 10^{-14}$, $h_t = 1 \cdot 10^{-16}$}
  \label{pic:implicit:9}
\end{figure}

\begin{figure}[!hbtp]
  \centering
  \includegraphics[width=\linewidth]{graph/solution/10}
  \caption{График решения по неявной схеме. $I = 10$, $J = 200$, $T = 4 \cdot 10^{-14}$, $h_t = 1 \cdot 10^{-17}$}
  \label{pic:implicit:10}
\end{figure}

Наконец, как и в разделе \ref{sec:explicit:emulation}, промоделируем процесс ещё дальше во времени и изобразим на рисунке \ref{pic:implicit:11} момент, когда волна уже возвращается, отразившись от стенки волновода.

\begin{figure}[!hbtp]
  \centering
  \includegraphics[width=\linewidth]{graph/solution/11}
  \caption{График решения по неявной схеме. $I = 10$, $J = 200$, $T = 1 \cdot 10^{-13}$, $h_t = 1 \cdot 10^{-17}$}
  \label{pic:implicit:11}
\end{figure}

\section{Вычислительный эксперимент}

Проделаем действия, описанные в разделе \ref{sec:explicit:research} для неявной схемы. Получаем график зависимости погрешности численного решения от мелкости сетки по времени (рис.~\ref{pic:implicit:research:1}), по оси $y$ (рис.~\ref{pic:implicit:research:2}) и по оси $z$ (рис.~\ref{pic:implicit:research:3}). Графики далеки от идеала, заданного представлением \eqref{eq:approx:imlicit}, что объясняется погрешностями, описаннымими в разделе  \ref{sec:explicit:research}.

\begin{figure}[!hbtp]
  \centering
  \includegraphics[width=\linewidth]{graph/research/5}
  \caption{Зависимость погрешности численного решения по неявной схеме от мелкости сетки по времени}
  \label{pic:implicit:research:1}
\end{figure}

\begin{figure}[!hbtp]
  \centering
  \includegraphics[width=\linewidth]{graph/research/6}
  \caption{Зависимость погрешности численного решения по неявной схеме от мелкости сетки по оси $y$}
  \label{pic:implicit:research:2}
\end{figure}

\begin{figure}[!hbtp]
  \centering
  \includegraphics[width=\linewidth]{graph/research/7}
  \caption{Зависимость погрешности численного решения по неявной схеме от мелкости сетки по оси $z$}
  \label{pic:implicit:research:3}
\end{figure}
